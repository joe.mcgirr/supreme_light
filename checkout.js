'use strict';

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// implement message passing here
function getSizeFromExtension(){
    return "Medium";
}

var size = getSizeFromExtension();

let checkoutButton = document.getElementsByName("commit")[0];
console.log(checkoutButton.getAttribute("value"))
if (checkoutButton.getAttribute("value") == "add to basket") {
    //select size
    let size = document.getElementById("size");
    console.log(size.getAttribute("type"))
    if (size.getAttribute("type") != "hidden"){
        console.log("size found");
        size.options[0].text = size;
        size.options[0].selected = true;
    }

    checkoutButton.click();
    sleep(2000).then(() => {document.getElementsByClassName("button checkout")[0].click();});
}