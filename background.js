// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

chrome.runtime.onInstalled.addListener(function() {
  chrome.storage.sync.set({color: '#3aa757'}, function() {
    console.log('The color is green.');
  });
});
// chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
//   chrome.tabs.create({ url: request.openUrlInEditor }, function(tab) {
//       chrome.tabs.executeScript(tab.id, {file: "checkout.js"});
//     });
//   });
