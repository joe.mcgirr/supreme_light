// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

// // item properties
// task1 = new SupremeTask("crew socks", "", "Accessories", "red")
// task1 = new SupremeTask("crew socks", "", "Accessories", "red")
var tasks = []
var keywords = "boxer briefs";
var size = "";
var category = "Accessories";
var colour = "red";
const mobileStockURL = "https://www.supremenewyork.com/mobile_stock.json";
const shopURL = "https://www.supremenewyork.com/shop/";
const checkoutURL = "https://www.supremenewyork.com/checkout";


function injectTheScript() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      // query the active tab, which will be only one tab
      //and inject the script in it
      console.log("tab id = " + tabs[3].id);
      
      //{code: 'document.body.style.backgroundColor = "' + color + '";'}
  });
}



let getItem = document.getElementById('getItem');

getItem.onclick = function(element) {
  // get JSON from mobile stock URL
  fetch(mobileStockURL)
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(data) {
        var all_products = data.products_and_categories;
        var product;
        for(product of all_products[category]){
          if(product.name.toLowerCase().includes(keywords.toLowerCase())){
            var productURL = shopURL+product.id;

            // script has to be injected while tab is inactive (else popup is closed before callback is invoked)
            chrome.tabs.create({ url: productURL, active: false}, function(tab) {
              console.log(tab)
              chrome.tabs.executeScript(tab.id, {file: "checkout.js"});
              //chrome.tabs.update(tab.id, {url: checkoutURL});
              // now tab can be set to active
              chrome.tabs.update(tab.id, {active: true});
            });
          }
        }
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
};
